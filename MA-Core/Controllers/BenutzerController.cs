﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_Core.Db;
using MA_Core.DbModell;
using MA_Core.Dto;
using Microsoft.AspNetCore.Mvc;


namespace MA_Core.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BenutzerController : Controller
    {
        [HttpPost]
        [ActionName("add-benutzer")]
        public ActionResult AddBenutzer([FromBody] BenutzerDto newBenutzer, [FromServices] Database db)
        {
            var benutzer = new Benutzer
            {
                Name = newBenutzer.Name,
                Passwort = newBenutzer.Passwort,
                Benutzername = newBenutzer.Benutzername,
                Rolle = newBenutzer.Rolle
            };

            var kunde = db.Kunden.SingleOrDefault(k => k.Name == newBenutzer.Kundenname);

            benutzer.Kunde = kunde;
            benutzer.KundeId = kunde.Id;

            db.Benutzer.Add(benutzer);

            db.SaveChanges();

            return Ok();
        }

    }
}
