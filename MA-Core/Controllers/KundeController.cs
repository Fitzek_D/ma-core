﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_Core.Db;
using MA_Core.DbModell;
using MA_Core.Dto;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_Core.Controllers
{
    [Route("api/[controller]/[action]")]
    public class KundeController : Controller
    {
        [HttpPost]
        [ActionName("add-kunde")]
        public ActionResult AddKunde([FromServices] Database db, [FromBody] KundeDto kundeDto)
        {
            var kunde = new Kunde {
                Name = kundeDto.Name
            };
            
            db.Kunden.Add(kunde);
            db.SaveChanges();
            
            return Ok();
        }
    }
}
