﻿using System;
namespace MA_Core.DbModell
{
    public class ZvooveBenutzer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Rolle { get; set; }
    }
}
