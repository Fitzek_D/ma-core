﻿using System;
namespace MA_Core.DbModell
{
    public class Lohn
    {
        public int Id { get; set; }
        public int Monat { get; set; }
        public int Jahr { get; set; }
        public int Betrag { get; set; }

        public Zeitarbeiter Zeitarbeiter { get; set; }

    }
}
