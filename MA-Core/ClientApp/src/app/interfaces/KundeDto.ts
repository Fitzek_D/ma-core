export interface KundeDto {
    benutzername: string,
    name: string,
    kundenname: string,
    passwort: string,
    rolle: string
}