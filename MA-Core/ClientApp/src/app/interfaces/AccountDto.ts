import { ProduktDto } from "./produktDto";

export interface AccountDto {
    validLogIn: boolean;
    benutzername: string;
    kundenname: string;
    rolle: string;
    produkte: ProduktDto[];
    id: number;
}