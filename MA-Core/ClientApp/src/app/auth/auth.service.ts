import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AccountDto } from '../interfaces/AccountDto';
import { LoginDto } from '../interfaces/LoginDto';
@Injectable({
    providedIn: 'root'
})
export class AuthService {
    isLoggedIn = false;
    redirectUrl: string | null = null;
    accountData: AccountDto;
    userSubject = new BehaviorSubject<AccountDto>(null);
    

    constructor(private http: HttpClient, private router: Router) { }
    
    login(loginData: LoginDto) {
        return this.http.post<AccountDto>('api/login/check-login', loginData).subscribe(account => {
            if(account.validLogIn){
                this.accountData = account;
                this.userSubject.next(account);
                console.log(account.rolle);
                console.log(account.produkte);
                this.isLoggedIn = true;
                if(this.redirectUrl) {
                    this.router.navigate([this.redirectUrl]);
                    this.redirectUrl = null;
                }
            }
        });
    }

    logout() {
        this.isLoggedIn = false;
    }
}