﻿using System;
using System.Collections.Generic;
using System.Linq;
using MA_Core.DbModell;

namespace MA_Core.Db
{
    public static class DbInit
    {

        public static void Initialize(Database dbContext)
        {
            dbContext.Database.EnsureCreated();

            if (!dbContext.ZvooveBenutzer.Any())
            {
                //zvooveBenutzer
                var zvooveBenutzer = new ZvooveBenutzer
                {
                    Name = "Dennis Fitzek",
                    Rolle = "Entwickler"
                };

                dbContext.Add(zvooveBenutzer);
                dbContext.SaveChanges();
            }


            if (!dbContext.Produkte.Any())
            {
                //Produkte
                var produkt = new Produkt
                {
                    Name = "zvoove PDL",
                    Kunden = new List<KundeProdukt>(),
                };

                var produkt2 = new Produkt
                {
                    Name = "zvoove Payroll",
                    Kunden = new List<KundeProdukt>(),
                };

                dbContext.Add(produkt);
                dbContext.Add(produkt2);
                dbContext.SaveChanges();
            }

            if (!dbContext.Kunden.Any())
            {
                //Kunde
                var kunde = new Kunde
                {
                    Name = "Masterarbeit AG",
                    Produkte = new List<KundeProdukt>(),
                    Benutzer = new List<Benutzer>(),
                    Auftraege = new List<Auftrag>()
                };

                var kunde2 = new Kunde
                {
                    Name = "Fitzek Corp.",
                    Produkte = new List<KundeProdukt>(),
                    Benutzer = new List<Benutzer>(),
                    Auftraege = new List<Auftrag>(),
                };

                dbContext.Add(kunde2);
                dbContext.Add(kunde);
                dbContext.SaveChanges();
            }

            var dbKunden = dbContext.Kunden.ToList();
            var dbProdukte = dbContext.Produkte.ToList();

            if (!dbContext.KundenProdukte.Any())
            {
                foreach (var kunde in dbKunden)
                {
                    if (kunde.Name == "Masterarbeit AG")
                    {
                        var produkt1 = dbProdukte.First(p => p.Name == "zvoove PDL");
                        var produkt2 = dbProdukte.First(p => p.Name == "zvoove Payroll");

                        var kundeprodukt = new KundeProdukt
                        {
                            Kunde = kunde,
                            Produkt = produkt1
                        };

                        var kundenprodukt2 = new KundeProdukt
                        {
                            Kunde = kunde,
                            Produkt = produkt2
                        };

                        dbContext.KundenProdukte.Add(kundeprodukt);
                        dbContext.KundenProdukte.Add(kundenprodukt2);
                        dbContext.SaveChanges();

                        var produktliste = new List<KundeProdukt>();

                        produktliste.Add(kundeprodukt);
                        produktliste.Add(kundenprodukt2);

                        kunde.Produkte = produktliste;

                        dbContext.Update(kunde);
                        dbContext.SaveChanges();
                    }

                    if (kunde.Name == "Fitzek Corp.")
                    {
                        var produkt1 = dbProdukte.First(p => p.Name == "zvoove PDL");

                        var kundeprodukt = new KundeProdukt
                        {
                            Kunde = kunde,
                            Produkt = produkt1
                        };

                        dbContext.KundenProdukte.Add(kundeprodukt);
                        dbContext.SaveChanges();

                        var produktliste = new List<KundeProdukt>();

                        produktliste.Add(kundeprodukt);

                        kunde.Produkte = produktliste;

                        dbContext.Update(kunde);
                        dbContext.SaveChanges();
                    }
                }

                var dbKundenProdukte = dbContext.KundenProdukte.ToList();

                foreach (var kunde in dbKunden)
                {
                    var kundenProdukte = dbKundenProdukte.First(kp => kp.Kunde.Name == "Masterarbeit AG" && kp.Produkt.Name == "zvoove PDL");
                    var kundenProdukte2 = dbKundenProdukte.First(kp => kp.Kunde.Name == "Masterarbeit AG" && kp.Produkt.Name == "zvoove Payroll");
                    var kundenProdukte3 = dbKundenProdukte.First(kp => kp.Kunde.Name == "Fitzek Corp." && kp.Produkt.Name == "zvoove PDL");

                    foreach (var produkt in dbProdukte)
                    {
                        if (produkt.Name == "zvoove PDL")
                        {
                            var kundenListe = new List<KundeProdukt>();
                            kundenListe.Add(kundenProdukte);
                            kundenListe.Add(kundenProdukte3);

                            produkt.Kunden = kundenListe;

                            dbContext.Update(produkt);
                            dbContext.SaveChanges();
                        }

                        if (produkt.Name == "zvoove Payroll")
                        {
                            var kundenListe = new List<KundeProdukt>();
                            kundenListe.Add(kundenProdukte2);

                            produkt.Kunden = kundenListe;

                            dbContext.Update(produkt);
                            dbContext.SaveChanges();
                        }
                    }
                }
            }


            if (!dbContext.Benutzer.Any())
            {
                var dbKunde = dbContext.Kunden.First(k => k.Name == "Masterarbeit AG");

                //Benutzer
                var benutzer = new Benutzer
                {
                    Name = "Thorsten van Hhalen",
                    Kunde = dbKunde,
                    Rolle = "Benutzer",
                    Passwort = "123"
                    
                };

                dbContext.Add(benutzer);
                dbContext.SaveChanges();

                dbKunde.Benutzer.Add(benutzer);

                dbContext.Update(dbKunde);
                dbContext.SaveChanges();

            }
        }
    }

}
