﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MA_Core.Migrations
{
    public partial class virtualAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Kunde",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kunde", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Produkt",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produkt", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ZvooveBenutzer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rolle = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZvooveBenutzer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Auftrag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Auftragsnummer = table.Column<int>(type: "int", nullable: false),
                    Bezeichnung = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Dauer = table.Column<int>(type: "int", nullable: false),
                    KundeId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auftrag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Auftrag_Kunde_KundeId",
                        column: x => x.KundeId,
                        principalTable: "Kunde",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Benutzer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Benutzername = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rolle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Passwort = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KundeId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Benutzer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Benutzer_Kunde_KundeId",
                        column: x => x.KundeId,
                        principalTable: "Kunde",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Zeitarbeiter",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KundeId = table.Column<int>(type: "int", nullable: true),
                    Gehalt = table.Column<int>(type: "int", nullable: false),
                    StundenImMonat = table.Column<int>(type: "int", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Handynummer = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zeitarbeiter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Zeitarbeiter_Kunde_KundeId",
                        column: x => x.KundeId,
                        principalTable: "Kunde",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "KundeProdukt",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KundeId = table.Column<int>(type: "int", nullable: true),
                    ProduktId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KundeProdukt", x => x.Id);
                    table.ForeignKey(
                        name: "FK_KundeProdukt_Kunde_KundeId",
                        column: x => x.KundeId,
                        principalTable: "Kunde",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_KundeProdukt_Produkt_ProduktId",
                        column: x => x.ProduktId,
                        principalTable: "Produkt",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lohn",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Monat = table.Column<int>(type: "int", nullable: false),
                    Jahr = table.Column<int>(type: "int", nullable: false),
                    Betrag = table.Column<int>(type: "int", nullable: false),
                    ZeitarbeiterId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lohn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lohn_Zeitarbeiter_ZeitarbeiterId",
                        column: x => x.ZeitarbeiterId,
                        principalTable: "Zeitarbeiter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ZeitarbeiterAuftrag",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ZeitarbeiterId = table.Column<int>(type: "int", nullable: true),
                    AuftragId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZeitarbeiterAuftrag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZeitarbeiterAuftrag_Auftrag_AuftragId",
                        column: x => x.AuftragId,
                        principalTable: "Auftrag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ZeitarbeiterAuftrag_Zeitarbeiter_ZeitarbeiterId",
                        column: x => x.ZeitarbeiterId,
                        principalTable: "Zeitarbeiter",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Auftrag_KundeId",
                table: "Auftrag",
                column: "KundeId");

            migrationBuilder.CreateIndex(
                name: "IX_Benutzer_KundeId",
                table: "Benutzer",
                column: "KundeId");

            migrationBuilder.CreateIndex(
                name: "IX_KundeProdukt_KundeId",
                table: "KundeProdukt",
                column: "KundeId");

            migrationBuilder.CreateIndex(
                name: "IX_KundeProdukt_ProduktId",
                table: "KundeProdukt",
                column: "ProduktId");

            migrationBuilder.CreateIndex(
                name: "IX_Lohn_ZeitarbeiterId",
                table: "Lohn",
                column: "ZeitarbeiterId");

            migrationBuilder.CreateIndex(
                name: "IX_Zeitarbeiter_KundeId",
                table: "Zeitarbeiter",
                column: "KundeId");

            migrationBuilder.CreateIndex(
                name: "IX_ZeitarbeiterAuftrag_AuftragId",
                table: "ZeitarbeiterAuftrag",
                column: "AuftragId");

            migrationBuilder.CreateIndex(
                name: "IX_ZeitarbeiterAuftrag_ZeitarbeiterId",
                table: "ZeitarbeiterAuftrag",
                column: "ZeitarbeiterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Benutzer");

            migrationBuilder.DropTable(
                name: "KundeProdukt");

            migrationBuilder.DropTable(
                name: "Lohn");

            migrationBuilder.DropTable(
                name: "ZeitarbeiterAuftrag");

            migrationBuilder.DropTable(
                name: "ZvooveBenutzer");

            migrationBuilder.DropTable(
                name: "Produkt");

            migrationBuilder.DropTable(
                name: "Auftrag");

            migrationBuilder.DropTable(
                name: "Zeitarbeiter");

            migrationBuilder.DropTable(
                name: "Kunde");
        }
    }
}
