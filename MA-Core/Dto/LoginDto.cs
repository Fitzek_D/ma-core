﻿using System;
namespace MA_Core.Dto
{
    public class LogInDto
    {
        public string Name { get; set; }
        public string Pw { get; set; }
    }

}
