﻿using System;
using System.Collections.Generic;

namespace MA_Core.Dto
{
    public class AccountDto
    {
        public bool ValidLogIn { get; set; }
        public string Benutzername { get; set; }
        public string Kundenname { get; set; }
        public string Rolle { get; set; }
        public int Id { get; set; }
        public IList<ProduktDto> Produkte { get; set; }

    }

}
