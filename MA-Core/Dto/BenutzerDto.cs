﻿using System;
namespace MA_Core.Dto
{
    public class BenutzerDto
    {
        public string Benutzername { get; set; }
        public string Name { get; set; }
        public string Kundenname { get; set; }
        public string Passwort { get; set; }
        public string Rolle { get; set; }
    }
}
